﻿/*
Program: Shared.cs
Purpose: Class for some misc shared variables
Revision History
    Matt Forgues - Dec 8 2018: Created
*/
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace Snake
{
    /// <summary>
    /// Shared class which holds misc shared variables
    /// </summary>
    class Shared
    {
        public static Vector2 stage;
        public static int highScore;
        public static StreamWriter writer;
        public static StreamReader reader;
        public static string filePath = $"{Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)}\\highscore.txt";
    }
}
