﻿/*
Program: SnakeGame.cs
Purpose: All the code for the SnakeGame
Revision History
    Matt Forgues - Dec 8 2018: Created
*/
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Snake
{
    /// <summary>
    /// SnakeGame class which inherits from DrawableGameComponent
    /// </summary>
    class SnakeGame : DrawableGameComponent
    {
        SpriteBatch spriteBatch;
        SpriteFont regularFont;
        List<Vector2> snakePosition = new List<Vector2>();
        List<Vector2> grid = new List<Vector2>();
        List<Vector2> walls = new List<Vector2>();
        Vector2 foodPos = new Vector2(-1, -1);
        private Texture2D tex;
        private Vector2 speed;
        private Random random;
        private int lastTime;
        private int score = 0;
        private bool paused = true;
        SoundEffect sound;
        /// <summary>
        /// SnakeGame constructor
        /// </summary>
        public SnakeGame(
            Game game,
            SpriteBatch spriteBatch,
            SpriteFont spriteFont,
            Texture2D tex,
            SoundEffect sound
            ) : base(game)
        {
            this.spriteBatch = spriteBatch;
            this.regularFont = spriteFont;
            this.tex = tex;
            this.sound = sound;
            this.random = new Random();
            Reset();

            for (int x = 0; x < Shared.stage.X / (tex.Width + 1); x++)
            {
                for (int y = 0; y < Shared.stage.Y / (tex.Width + 1); y++)
                {
                    grid.Add(new Vector2(x, y));
                }
            }
            

        }
        /// <summary>
        /// Writes the current score to the highscore.txt, if the score was higher than the previous highscore
        /// </summary>
        private void WriteHighscore()
        {
            try
            {
                Shared.highScore = score;
                using (Shared.writer = new StreamWriter(Shared.filePath, false))
                {
                    Shared.writer.WriteLine(score);
                }
            }
            catch (Exception ex)
            {
            }
            

        }
        /// <summary>
        /// GeneratesWalls if the user has specified they want walls
        /// </summary>
        private void GenerateWalls()
        {
            for (int j = 0; j < GameSettings.numberOfWalls; j++)
            {
                walls.Add(new Vector2(
                                    random.Next(0, (int)Shared.stage.X) / (tex.Width + 1),
                                    random.Next(0, (int)Shared.stage.Y / (tex.Width + 1))));

                for (int i = 0; i < GameSettings.blocksPerWall; i++)
                {
                    int nextPos = random.Next(0, 4);
                    if (nextPos == 0)
                    {
                        walls.Add(new Vector2(walls[walls.Count - 1].X, walls[walls.Count - 1].Y - 1));
                    }
                    else if (nextPos == 1)
                    {
                        walls.Add(new Vector2(walls[walls.Count - 1].X - 1, walls[walls.Count - 1].Y));
                    }
                    else if (nextPos == 2)
                    {
                        walls.Add(new Vector2(walls[walls.Count - 1].X, walls[walls.Count - 1].Y + 1));
                    }
                    else if (nextPos == 3)
                    {
                        walls.Add(new Vector2(walls[walls.Count - 1].X - 1, walls[walls.Count - 1].Y - 1));
                    }
                }
            }

        }
        /// <summary>
        /// Resets the game 
        /// </summary>
        private void Reset()
        {
            if (score > Shared.highScore)
            {
                WriteHighscore();
            }
            paused = true;
            snakePosition = new List<Vector2>();
            walls = new List<Vector2>();
            foodPos = new Vector2(
                    random.Next(0, (int)Shared.stage.X) / (tex.Width + 1),
                    random.Next(0, (int)Shared.stage.Y / (tex.Width + 1)));
            GenerateWalls();
            int meme = ((int)Shared.stage.X / (tex.Width + 1));
            int startX = random.Next(5, ((int)Shared.stage.X / (tex.Width + 1)) - 5);
            int startY = random.Next(5, ((int)Shared.stage.Y / (tex.Width + 1)) - 5);
            int startDir = random.Next(0, 4); // 0 = up, 1 = left, 2 = down, 3 = right
            switch (startDir)
            {
                case 0:
                    for (int i = 0; i < GameSettings.startingLength; i++)
                    {
                        snakePosition.Add(new Vector2(startX, startY + i));
                        speed = new Vector2(0, -1);
                    }
                    break;
                case 1:
                    for (int i = 0; i < GameSettings.startingLength; i++)
                    {
                        snakePosition.Add(new Vector2(startX + i, startY));
                        speed = new Vector2(-1, 0);
                    }
                    break;
                case 2:
                    for (int i = 0; i < GameSettings.startingLength; i++)
                    {
                        snakePosition.Add(new Vector2(startX, startY - i));
                        speed = new Vector2(0, 1);
                    }
                    break;
                case 3:
                    for (int i = 0; i < GameSettings.startingLength; i++)
                    {
                        snakePosition.Add(new Vector2(startX - i, startY));
                        speed = new Vector2(1, 0);
                    }
                    break;
            }

        }
        /// <summary>
        /// Draw override for the SnakeGame class. This draws the grid, snake, food and any walls
        /// </summary>
        public override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(GameSettings.backgroundColour);

            spriteBatch.Begin();
            
            foreach(Vector2 item in grid)
            {
                spriteBatch.Draw(tex, new Rectangle((int)item.X * (tex.Width + 1), (int)item.Y * (tex.Width + 1), tex.Height, tex.Width), GameSettings.gridColour);
            }
            foreach (Vector2 item in walls)
            {
                spriteBatch.Draw(tex, new Rectangle((int)item.X * (tex.Width + 1), (int)item.Y * (tex.Width + 1), tex.Width, tex.Height), GameSettings.wallColour);
            }
            foreach (Vector2 snake in snakePosition)
            {
                if (snakePosition.IndexOf(snake) == 0)
                {
                    spriteBatch.Draw(tex, new Rectangle((int)snake.X * (tex.Width + 1), (int)snake.Y * (tex.Width + 1), tex.Width, tex.Height), GameSettings.snakeHeadColour);
                }
                else
                {
                    spriteBatch.Draw(tex, new Rectangle((int)snake.X * (tex.Width + 1), (int)snake.Y * (tex.Width + 1), tex.Width, tex.Height), GameSettings.snakeColour);
                }
            }
            spriteBatch.Draw(tex, new Rectangle((int)foodPos.X * (tex.Width + 1), (int)foodPos.Y * (tex.Width + 1), tex.Width, tex.Height), GameSettings.foodColour);
            score = (snakePosition.Count() - GameSettings.startingLength);
            spriteBatch.DrawString(regularFont, $"Score: {score.ToString()}", new Vector2(0, 0), GameSettings.scoreColour, 0f, new Vector2(0, 0), 2.0f, SpriteEffects.None, 0f);
            spriteBatch.End();
            
            base.Draw(gameTime);
        }
        /// <summary>
        /// Update override for the SnakeGame class. This moves the snake, checks if the food was grabbed, and if the user died
        /// </summary>
        public override void Update(GameTime gameTime)
        {         

            if (paused)
            {
                KeyboardState ks = Keyboard.GetState();

                if (ks.IsKeyDown(Keys.Up) && speed.Y != 1)
                {
                    speed = new Vector2(0, -1);
                    paused = false;
                }
                else if (ks.IsKeyDown(Keys.Left) && speed.X != 1)
                {
                    speed = new Vector2(-1, 0);
                    paused = false;
                }
                else if (ks.IsKeyDown(Keys.Down) && speed.Y != -1)
                {
                    speed = new Vector2(0, 1);
                    paused = false;
                }
                else if (ks.IsKeyDown(Keys.Right) && speed.X != -1)
                {
                    speed = new Vector2(1, 0);
                    paused = false;
                }

            }
            else
            {
                if (gameTime.TotalGameTime.TotalMilliseconds > lastTime + GameSettings.gameTick)
                {
                    KeyboardState ks = Keyboard.GetState();

                    if (ks.IsKeyDown(Keys.Up) && speed.Y != 1)
                    {
                        speed = new Vector2(0, -1);
                    }
                    else if (ks.IsKeyDown(Keys.Left) && speed.X != 1)
                    {
                        speed = new Vector2(-1, 0);
                    }
                    else if (ks.IsKeyDown(Keys.Down) && speed.Y != -1)
                    {
                        speed = new Vector2(0, 1);
                    }
                    else if (ks.IsKeyDown(Keys.Right) && speed.X != -1)
                    {
                        speed = new Vector2(1, 0);
                    }
                    else if(ks.IsKeyDown(Keys.P))
                    {
                        paused = true;
                    }

                    for (int i = snakePosition.Count - 1; i > 0; i--)
                    {
                        snakePosition[i] = snakePosition[i - 1];
                    }
                    snakePosition[0] += speed;


                    lastTime = (int)gameTime.TotalGameTime.TotalMilliseconds;
                }

                if (snakePosition[0] == foodPos)
                {
                    if (snakePosition.Count() == grid.Count())
                        Reset();
                    else
                    {
                        List<Vector2> emptyTiles = new List<Vector2>();

                        emptyTiles = grid.Except(walls).Except(snakePosition).ToList();
                        foodPos = emptyTiles[random.Next(0, emptyTiles.Count())];

                        for (int i = 0; i < GameSettings.lengthPerFood; i++)
                        {
                            snakePosition.Add(snakePosition[snakePosition.Count - 1]);
                        }
                        sound.Play(GameSettings.scoreVolume, 0.0f, 0.0f);
                    }
                    

                }

                //for (int i = 1; i < snakePosition.Count; i++)
                //{
                //    if (snakePosition[i] == snakePosition[0])
                //    {
                //        Reset();
                //    }
                //}

                if (snakePosition.Count(x => x == snakePosition[0]) == 2)
                {
                    Reset();
                }
                if (!grid.Contains(snakePosition[0]))
                {
                    Reset();
                }
                if (walls.Contains(snakePosition[0]))
                {
                    Reset();
                }
            }
            

            base.Update(gameTime);
        }
    }
}
