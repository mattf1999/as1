﻿/*
Program: ActionScene.cs
Purpose: Starts the game
Revision History
    Matt Forgues - Dec 8 2018: Created
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Snake
{
    /// <summary>
    /// ActionScene class which inherits from GameScene
    /// </summary>
    class ActionScene : GameScene
    {
        private SpriteBatch spriteBatch;
        private SnakeGame snake;
        private Texture2D tex;
        private SpriteFont spriteFont;
        private SoundEffect sound;
        /// <summary>
        /// ActionScene constructor
        /// </summary>
        public ActionScene(
            Game game,
            SpriteBatch spriteBatch,
            Texture2D tex,
            SpriteFont regularFont
            ) : base(game)
        {
            this.spriteBatch = spriteBatch;
            this.tex = tex;
            this.spriteFont = regularFont;
            this.sound = game.Content.Load<SoundEffect>(@"Sound\beep");
            snake = new SnakeGame(game, spriteBatch,regularFont, tex, sound);
            this.Components.Add(snake);
        }
    }
}
