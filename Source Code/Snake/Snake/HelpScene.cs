﻿/*
Program: HelpScene.cs
Purpose: Shows the help page for this game
Revision History
    Matt Forgues - Dec 8 2018: Created
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
namespace Snake
{
    /// <summary>
    /// HelpScene class which inherits from GameScene
    /// </summary>
    class HelpScene : GameScene
    {
        SpriteBatch spriteBatch;
        SpriteFont regularFont;
        /// <summary>
        /// HelpScene constructor
        /// </summary>
        public HelpScene(
            Game game,
            SpriteBatch spriteBatch,
            SpriteFont regularFont
            ) : base(game)
        {
            this.spriteBatch = spriteBatch;
            this.regularFont = regularFont;
        }
        /// <summary>
        /// Draw override for the HelpScene class
        /// </summary>
        public override void Draw(GameTime gameTime)
        {
            spriteBatch.Begin();
            string text = "The purpose of this game is to get as many points as you can without hitting your own tail, or the boundries." +
                "\nYou get more points by picking up the coloured block on the grid, which will increase your snakes length." +
                "\nTo play this game, you use the arrow keys on your keyboard to dictate where the head of the snake maneuvers." +
                "\n\nControls:" +
                "\nEsc: Main Menu" +
                "\nArrow Keys: Movement" +
                "\nP: Pause Game" +
                "\nM: Stop/Start Music";
            spriteBatch.DrawString(regularFont, text, new Vector2(0, 0), GameSettings.fontColour);
            spriteBatch.End();
            base.Draw(gameTime);
        }
        /// <summary>
        /// Update override for the HelpScene class
        /// </summary>
        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }
    }
}
