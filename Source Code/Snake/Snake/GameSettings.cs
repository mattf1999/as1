﻿/*
Program: GameSettings.cs
Purpose: Stores all the game settings for snake
Revision History
    Matt Forgues - Dec 8 2018: Created
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
namespace Snake
{
    /// <summary>
    /// GameSettings class which holds all game settings
    /// </summary>
    class GameSettings
    {
        public static Color backgroundColour = Color.Black;
        public static Color foodColour = Color.Blue;
        public static Color snakeColour = Color.Gray;
        public static Color snakeHeadColour = Color.Red;
        public static Color gridColour = Color.White;
        public static Color fontColour = Color.White;
        public static Color scoreColour = Color.DarkRed;
        public static Color wallColour = Color.Black;

        public static int gameTick = 80;
        public static int lengthPerFood = 1;
        public static int numberOfWalls = 0;
        public static int blocksPerWall = 5;
        public static int startingLength = 2;
        public static float scoreVolume = 0.5f;
        public static float musicVolume = 0.1f;

    }
}
