﻿/*
Program: HighscoreScene.cs
Purpose: Shows the current local highscore
Revision History
    Matt Forgues - Dec 8 2018: Created
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Snake
{
    /// <summary>
    /// HighscoreScene class which inherits from GameScene
    /// </summary>
    class HighscoreScene : GameScene
    {
        SpriteBatch spriteBatch;
        SpriteFont regularFont;
        public HighscoreScene(
            Game game,
            SpriteBatch spriteBatch,
            SpriteFont regularFont
            ) : base(game)
        {
            this.spriteBatch = spriteBatch;
            this.regularFont = regularFont;
        }
        /// <summary>
        /// Draw override for the HighscoreScene class
        /// </summary>
        public override void Draw(GameTime gameTime)
        {
            spriteBatch.Begin();
            spriteBatch.DrawString(regularFont, $"Highscore: {Shared.highScore}", new Vector2(0, 0), GameSettings.fontColour, 0f, new Vector2(0, 0), 2.0f, SpriteEffects.None, 0f);
            spriteBatch.End();
            base.Draw(gameTime);
        }
        /// <summary>
        /// Update override for the HighscoreScene class
        /// </summary>
        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }
    }
}
