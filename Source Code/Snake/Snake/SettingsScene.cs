﻿/*
Program: SettingsScene.cs
Purpose: Allows the user to change all the game settings (does not work currently, and is not being used)
Revision History
    Matt Forgues - Dec 8 2018: Created
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Snake
{
    /// <summary>
    /// SettingsScene class which inherits from GameScene. This allows changes to the game settings (not implemented)
    /// </summary>
    class SettingsScene : GameScene
    {
        public MenuComponent Menu { get; set; }
        SpriteBatch spriteBatch;
        SpriteFont regularFont;
        SpriteFont highlightFont;
        
        private string[] menuItems =
        {
            "Background Colour",
            "Food Colour",
            "Snake Colour",
            "Grid Colour",
            "Length per Food",
            "Starting Length",
            "Number of Walls",
            "Blocks Per Wall",
            "Tickrate"
        };
        
        /// <summary>
        /// SettingsScene constructor
        /// </summary>
        public SettingsScene(
            Game game,
            SpriteBatch spriteBatch,
            SpriteFont regularFont,
            SpriteFont highlightFont
            ) : base(game)
        {
            this.spriteBatch = spriteBatch;
            this.regularFont = regularFont;
            this.highlightFont = highlightFont;
            
            Menu = new MenuComponent(game, spriteBatch, regularFont, highlightFont, menuItems);
            this.Components.Add(Menu);

        }
        /// <summary>
        /// Draw override for the SettingsScene class
        /// </summary>
        public override void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);
        }
        /// <summary>
        /// Update override for the SettingsScene class
        /// </summary>
        public override void Update(GameTime gameTime)
        {
            KeyboardState ks = Keyboard.GetState();
            int selectedIndex = 0;

            selectedIndex = this.Menu.SelectedIndex;
            if (selectedIndex == 0 && ks.IsKeyDown(Keys.Enter))
            {
                GameSettings.backgroundColour = Color.Blue;
            }
            else if (selectedIndex == 1 && ks.IsKeyDown(Keys.Enter))
            {

            }
            else if (selectedIndex == 5 && ks.IsKeyDown(Keys.Enter))
            {
                
            }
            

            base.Update(gameTime);
        }
    }
}
