﻿/*
Program: AboutScene.cs
Purpose: Show the about page for this game
Revision History
    Matt Forgues - Dec 8 2018: Created

*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
namespace Snake
{
    /// <summary>
    /// AboutScene class which inherits from GameScene
    /// </summary>
    class AboutScene : GameScene
    {
        SpriteBatch spriteBatch;
        SpriteFont regularFont;
        /// <summary>
        /// AboutScene constructor
        /// </summary>
        public AboutScene(
            Game game,
            SpriteBatch spriteBatch,
            SpriteFont regularFont
            ) : base(game)
        {
            this.spriteBatch = spriteBatch;
            this.regularFont = regularFont;
        }
        /// <summary>
        /// Draw override for the AboutScene class
        /// </summary>
        public override void Draw(GameTime gameTime)
        {
            spriteBatch.Begin();
            string text = "Snake Game" +
                "\nRecreated by: Mathew Forgues";
            spriteBatch.DrawString(regularFont, text, new Vector2(0, 0), GameSettings.fontColour);
            spriteBatch.End();
            base.Draw(gameTime);
        }
        /// <summary>
        /// Update override for the AboutScene class
        /// </summary>
        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }
    }
}
