﻿/*
Program: Game1.cs
Purpose: Game window for snake
Revision History
    Matt Forgues - Dec 8 2018: Created
*/
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Media;
using System.Reflection;

namespace Snake
{
    /// <summary>
    /// Game1 class which holds basic game code
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        SpriteFont regularFont;
        SpriteFont highlightFont;
        Texture2D tex;
        Song music;
        bool musicPaused = false;
        private StartScene startScene;
        private ActionScene actionScene;
        private HelpScene helpScene;
        private HighscoreScene highscoreScene;
        private AboutScene aboutScene;
        private KeyboardState oldState;
        //private SettingsScene settingsScene;
        /// <summary>
        /// Default constructor for Game1, which sets up settings for the game
        /// </summary>
        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            this.IsMouseVisible = true;
            this.Window.AllowUserResizing = true;
            
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            Shared.stage = new Vector2(graphics.PreferredBackBufferWidth, graphics.PreferredBackBufferHeight);
            
            base.Initialize();

        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here
            tex = this.Content.Load<Texture2D>(@"Images\square");
            regularFont = this.Content.Load<SpriteFont>(@"Fonts\highlightfont");
            highlightFont = this.Content.Load<SpriteFont>(@"Fonts\highlightfont");
            music = this.Content.Load<Song>(@"Sound\music");

            int tilesX = (int)Math.Ceiling(Shared.stage.X / (tex.Width + 1));
            int tilesY = (int)Math.Ceiling(Shared.stage.Y / (tex.Width + 1));

            graphics.PreferredBackBufferWidth = tilesX * (tex.Width + 1);
            graphics.PreferredBackBufferHeight = tilesY * (tex.Height + 1);
            graphics.ApplyChanges();
            MediaPlayer.Volume = GameSettings.musicVolume;
            MediaPlayer.Play(music);
            
            startScene = new StartScene(this, spriteBatch);
            this.Components.Add(startScene);

            actionScene = new ActionScene(this, spriteBatch, tex, regularFont);
            this.Components.Add(actionScene);

            helpScene = new HelpScene(this, spriteBatch, regularFont);
            this.Components.Add(helpScene);

            highscoreScene = new HighscoreScene(this, spriteBatch, regularFont);
            this.Components.Add(highscoreScene);

            aboutScene = new AboutScene(this, spriteBatch, regularFont);
            this.Components.Add(aboutScene);

            try
            {
                using (Shared.reader = new StreamReader(Shared.filePath))
                {
                    Shared.highScore = int.Parse(Shared.reader.ReadLine());
                }
            }
            catch (Exception)
            {
            }
            


            //settingsScene = new SettingsScene(this, spriteBatch);
            //this.Components.Add(settingsScene);
            startScene.show();
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {

            // TODO: Add your update logic here
            //stage = new Vector2(graphics.PreferredBackBufferWidth, graphics.PreferredBackBufferHeight);
            if (MediaPlayer.State == MediaState.Stopped && !musicPaused)
                MediaPlayer.Play(music);

            KeyboardState ks = Keyboard.GetState();
            int selectedIndex = 0;

            if (startScene.Enabled)
            {
                selectedIndex = startScene.Menu.SelectedIndex;
                if (ks.IsKeyDown(Keys.Enter))
                {
                    
                    switch (selectedIndex)
                    {
                        case 0:
                            hideAllScenes();
                            actionScene.show();
                            break;
                        case 1:
                            hideAllScenes();
                            helpScene.show();
                            break;
                        case 2:
                            hideAllScenes();
                            highscoreScene.show();
                            break;
                        case 3:
                            hideAllScenes();
                            aboutScene.show();
                            break;
                        case 4:
                            Exit();
                            break;
                    }
                }
            }

            if (actionScene.Enabled)
            {
                if (ks.IsKeyDown(Keys.Escape))
                {
                    hideAllScenes();
                    this.Components.Remove(actionScene);
                    actionScene.Dispose();
                    actionScene = new ActionScene(this, spriteBatch, tex, regularFont);
                    this.Components.Add(actionScene);
                    startScene.show();
                }
            }
            if (helpScene.Enabled || highscoreScene.Enabled || aboutScene.Enabled)
            {
                if (ks.IsKeyDown(Keys.Escape))
                {
                    hideAllScenes();
                    startScene.show();
                }
            }
            if (ks.IsKeyDown(Keys.M) && oldState.IsKeyUp(Keys.M))
            {
                if (MediaPlayer.State == MediaState.Paused)
                {
                    musicPaused = false;
                    MediaPlayer.Play(music);
                }
                else
                {
                    musicPaused = true;
                    MediaPlayer.Pause();
                }      
            }
            oldState = ks;

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(GameSettings.backgroundColour);

            // TODO: Add your drawing code here

            base.Draw(gameTime);
        }
        /// <summary>
        /// Hides all scenes
        /// </summary>
        private void hideAllScenes()
        {
            foreach (GameScene item in Components)
            {
                item.hide();
            }
        }
    }
}
