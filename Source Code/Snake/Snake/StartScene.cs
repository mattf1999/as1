﻿/*
Program: StartScene.cs
Purpose: Displays the main menu
Revision History
    Matt Forgues - Dec 8 2018: Created
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Snake
{
    /// <summary>
    /// StartScene class that inherits from GameScene
    /// </summary>
    class StartScene : GameScene
    {
        public MenuComponent Menu { get; set; }

        private SpriteBatch spriteBatch;
        private string[] menuItems =
        {
            "Start Game",
            "Help",
            "High Score",
            "About",
            "Quit"
        };
        /// <summary>
        /// StartScene constructor
        /// </summary>
        public StartScene(Game game,
           SpriteBatch spriteBatch) : base(game)
        {
            this.spriteBatch = spriteBatch;
            SpriteFont regularFont = game.Content.Load<SpriteFont>(@"Fonts\regularfont");
            SpriteFont highlightFont = game.Content.Load<SpriteFont>(@"Fonts\highlightfont");
            Menu = new MenuComponent(game, spriteBatch, regularFont, highlightFont, menuItems);
            this.Components.Add(Menu);
        }
        /// <summary>
        /// Draw override for the StartScene
        /// </summary>
        public override void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);
        }
        /// <summary>
        /// Update ovverride for the StartScene
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }
    }
}
